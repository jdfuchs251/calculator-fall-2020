﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using JDF.BasicCalculator.BL;

namespace JDF.BasicCalculator.UI
{
    enum OperatorType
    {
        None, Add, Subtract, Multiply, Divide, Sqrt, Reciprocal // Values correspond with button names
    }

    public partial class frmCalculator : Form
    {
        // Set up form-level variables
        Calculator calc = new Calculator(); // Used to run calculations
        OperatorType operation; // Tracks operation for compound calculations
        bool clearDisplay; // Whether or not display should be cleared upon next entry
        string strFirstNum; // Holds a number fromm a calculation
        string strSecondNum; // Holds a second number from a calculation (only used in compound calculations)

        public frmCalculator()
        {
            InitializeComponent();
            ClearAll();
        }

        /**********************************************************************************
        /* Name: btnClear_Click
        /* Description: Clears all stored data and resets the display upon click
        /*********************************************************************************/
        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearAll();
        }

        /**********************************************************************************
        /* Name: ClearAll
        /* Description: Clears all stored data and resets the display
        /*********************************************************************************/
        private void ClearAll()
        {
            ClearStoredValues();
            lblDisplay.Text = "0";
            clearDisplay = true;
        }

        /**********************************************************************************
        /* Name: ClearStoredValues
        /* Description: Clears stored numeric strings and operator
        /*********************************************************************************/
        private void ClearStoredValues()
        {
            strFirstNum = string.Empty;
            strSecondNum = string.Empty;
            operation = OperatorType.None;
        }

        /**********************************************************************************
        /* Name: NumberKey_Click
        /* Description: Handles display when user clicks a number button
        /*********************************************************************************/
        private void NumberKey_Click(object sender, EventArgs e)
        {
            if (lblDisplay.Text.Contains("ERROR")) return; // Do nothing if an error is in the display
            if (strSecondNum != string.Empty) ClearStoredValues(); // Start a new calculation
            string number = ((Button)sender).Name.Substring(3); // Grab the last character of the button name
            if (lblDisplay.Text == "0" && number == "0") return; // Don't keep appending 0's
            if (clearDisplay) lblDisplay.Text = number;
            else if (lblDisplay.Text == "-" || lblDisplay.Text == "-0") lblDisplay.Text = "-" + number;
            else lblDisplay.Text += number;
            clearDisplay = (lblDisplay.Text == "0");
        }

        /**********************************************************************************
        /* Name: btnSign_Click
        /* Description: Handles display when user clicks the +/- button
        /*********************************************************************************/
        private void btnSign_Click(object sender, EventArgs e)
        {
            if (lblDisplay.Text.Contains("ERROR")) return; // Do nothing if an error is in the display
            if (strSecondNum != string.Empty) ClearStoredValues(); // Start a new calculation
            if (clearDisplay) lblDisplay.Text = "-0"; // Prepend negative sign to 0
            else if (lblDisplay.Text.Contains("-"))  // Remove negative sign if already negative
                lblDisplay.Text = lblDisplay.Text.Substring(1, lblDisplay.Text.Length - 1);
            else lblDisplay.Text = "-" + lblDisplay.Text; // Prepend negative sign
            clearDisplay = (lblDisplay.Text == "0");
        }

        /**********************************************************************************
        /* Name: btnDot_Click
        /* Description: Handles display when user clicks the . button
        /*********************************************************************************/
        private void btnDot_Click(object sender, EventArgs e)
        {
            if (lblDisplay.Text.Contains("ERROR")) return; // Do nothing if an error is in the display
            if (strSecondNum != string.Empty) ClearStoredValues(); // Start a new calculation
            if (clearDisplay) lblDisplay.Text = "0.";
            if (!lblDisplay.Text.Contains(".")) lblDisplay.Text += "."; // Don't add more than 1 period
            clearDisplay = false;
        }

        /**********************************************************************************
        /* Name: btnBack_Click
        /* Description: Handles display when user clicks the Back button
        /*********************************************************************************/
        private void btnBack_Click(object sender, EventArgs e)
        {
            if (lblDisplay.Text.Contains("ERROR")) return; // Do nothing if an error is in the display
            if (clearDisplay) lblDisplay.Text = "0"; // Reset so user is not confused
            lblDisplay.Text = lblDisplay.Text.Substring(0, lblDisplay.Text.Length - 1);
            if (lblDisplay.Text.Length == 0 || lblDisplay.Text == "-" || lblDisplay.Text == "-0")
                lblDisplay.Text = "0";
            clearDisplay = (lblDisplay.Text == "0");
        }

        /**********************************************************************************
        /* Name: SimpleOperator_Click
        /* Description: Called when user clicks sqrt or 1/x buttons. Performs the selected
        /*   operation and displays the result. Caches result for further operations.
        /*********************************************************************************/
        private void SimpleOperator_Click(object sender, EventArgs e)
        {
            if (lblDisplay.Text.Contains("ERROR")) return; // Do nothing if an error is in the display
            OperatorType opClicked = (OperatorType)Enum.Parse(typeof(OperatorType), ((Button)sender).Name.Substring(3));
            clearDisplay = true; // Clear on next entry regardless of whether operation succeeds
            try
            {
                double number = calc.ConvertStringToNumber(lblDisplay.Text);
                if (opClicked == OperatorType.Sqrt) lblDisplay.Text = calc.Sqrt(number).ToString();
                if (opClicked == OperatorType.Reciprocal) lblDisplay.Text = calc.Reciprocal(number).ToString();
                strFirstNum = lblDisplay.Text;
            }
            catch (Exception ex)
            {
                lblDisplay.Text = ex.Message;
                ClearStoredValues();
            }
        }

        /**********************************************************************************
        /* Name: CompoundOperator_Click
        /* Description: Called when user clicks +, -, x, or / buttons. Performs the selected
        /*   operation and displays the result. Caches result for further operations.
        /*********************************************************************************/
        private void CompoundOperator_Click(object sender, EventArgs e)
        {
            if (lblDisplay.Text.Contains("ERROR")) return; // Do nothing if an error is in the display
            if (strSecondNum != string.Empty) ClearStoredValues(); // Operator clicked after equals
            if (strFirstNum != string.Empty) RunCompoundCalculation(strFirstNum, lblDisplay.Text, operation); // operation is always set with strFirstNum
            if (!lblDisplay.Text.Contains("ERROR")) strFirstNum = lblDisplay.Text;
            operation = (OperatorType)Enum.Parse(typeof(OperatorType), ((Button)sender).Name.Substring(3));
            clearDisplay = true;
        }

        /**********************************************************************************
        /* Name: btnEquals_Click
        /* Description: Called when user clicks the = button. Performs the stored operation.
        /*********************************************************************************/
        private void btnEquals_Click(object sender, EventArgs e)
        {
            if (lblDisplay.Text.Contains("ERROR")) return; // Do nothing if an error is in the display
            if (strFirstNum == string.Empty) return; // No calculation to do
            if (strSecondNum == string.Empty) // Did not previously click =, use display for second number
            {
                strSecondNum = lblDisplay.Text; // Store second number in case = is clicked again
                RunCompoundCalculation(strFirstNum, lblDisplay.Text, operation);
            }
            else RunCompoundCalculation(strFirstNum, strSecondNum, operation); // Previously clicked =, use stored second number
            if (!lblDisplay.Text.Contains("ERROR")) strFirstNum = lblDisplay.Text;
            clearDisplay = true;
        }

        /**********************************************************************************
        /* Name: RunCompoundCalculation
        /* Description: Generic method to run a compound calculation (+, -, x, /, or =)
        /* Parameters:
        /*      string first - First number in the compound calculation (as a string)
        /*     string second - Second number in the compound calculation (as a string)
        /*   OperatorType op - Which compound operation to run
        /*********************************************************************************/
        private void RunCompoundCalculation(string first, string second, OperatorType op)
        {
            try
            {
                double firstNum = calc.ConvertStringToNumber(first);
                double secondNum = calc.ConvertStringToNumber(second);
                double result = 0;
                if (op == OperatorType.Add) result = calc.Add(firstNum, secondNum);
                if (op == OperatorType.Subtract) result = calc.Subtract(firstNum, secondNum);
                if (op == OperatorType.Multiply) result = calc.Multiply(firstNum, secondNum);
                if (op == OperatorType.Divide) result = calc.Divide(firstNum, secondNum);
                lblDisplay.Text = result.ToString();
            }
            catch (Exception ex)
            {
                lblDisplay.Text = ex.Message;
                ClearStoredValues();
            }
        }
    }
}
