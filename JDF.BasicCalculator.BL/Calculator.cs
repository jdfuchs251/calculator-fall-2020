﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JDF.BasicCalculator.BL
{
    /**********************************************************************************
    /* Name: Calculator
    /* Description: Holds the methods used to perform basic calculator operations
    /*********************************************************************************/
    public class Calculator
    {
        /**********************************************************************************
        /* Name: Add
        /* Description: Performs the addition operation
        /* Parameters:
        /*    double firstNum - The first number to use in the calculation
        /*   double secondNum - The second number to use in the calculation
        /* Returns: (double) firstNum + secondNum
        /*********************************************************************************/
        public double Add(double firstNum, double secondNum)
        {
            return firstNum + secondNum;
        }

        /**********************************************************************************
        /* Name: Subtract
        /* Description: Performs the subtraction operation
        /* Parameters:
        /*    double firstNum - The first number to use in the calculation
        /*   double secondNum - The second number to use in the calculation
        /* Returns: (double) firstNum - secondNum
        /*********************************************************************************/
        public double Subtract(double firstNum, double secondNum)
        {
            return firstNum - secondNum;
        }

        /**********************************************************************************
        /* Name: Multiply
        /* Description: Performs the multiplication operation
        /* Parameters:
        /*    double firstNum - The first number to use in the calculation
        /*   double secondNum - The second number to use in the calculation
        /* Returns: (double) firstNum * secondNum
        /*********************************************************************************/
        public double Multiply(double firstNum, double secondNum)
        {
            return firstNum * secondNum;
        }

        /**********************************************************************************
        /* Name: Divide
        /* Description: Performs the division operation
        /* Parameters:
        /*    double firstNum - The first number to use in the calculation
        /*   double secondNum - The second number to use in the calculation
        /* Returns: (double) firstNum / secondNum. Throws an exception if dividing by 0.
        /*********************************************************************************/
        public double Divide(double firstNum, double secondNum)
        {
            if (secondNum == 0) throw new Exception("ERROR: Divide by 0");
            return firstNum / secondNum;
        }

        /**********************************************************************************
        /* Name: Sqrt
        /* Description: Performs the square root operation
        /* Parameters:
        /*   double number - The number to use in the calculation
        /* Returns: (double) Sqrt(number). Throws an exception if taking sqrt of a negative number.
        /*********************************************************************************/
        public double Sqrt(double number)
        {
            if (number < 0) throw new Exception("ERROR: Sqrt of negative");
            return Math.Sqrt(number);
        }

        /**********************************************************************************
        /* Name: Reciprocal
        /* Description: Calculates the reciprocal of a number
        /* Parameters:
        /*   double number - The number to use in the calculation
        /* Returns: (double) 1 / number. Throws an exception if dividing by 0.
        /*********************************************************************************/
        public double Reciprocal(double number)
        {
            if (number == 0) throw new Exception("ERROR: Divide by 0");
            return 1 / number;
        }

        /**********************************************************************************
        /* Name: ConvertStringToNumber
        /* Description: Used to convert calculator display values (strings) to numbers
        /*   that can be used to perform calculations.
        /* Parameters:
        /*   string number - Numeric string to convert to a number
        /* Returns: Numeric value as a double. Throws an exception if string cannot be
        /*   converted (this should not happen in the basic calculator application if
        /*   the display is handled correctly)
        /*********************************************************************************/
        public double ConvertStringToNumber(string strNumber)
        {
            if (strNumber.Contains(".")) strNumber += "0";
            if (!Double.TryParse(strNumber, out double number)) throw new Exception("ERROR: Invalid entry");
            return number;
        }
    }
}
